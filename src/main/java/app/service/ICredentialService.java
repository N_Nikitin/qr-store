package app.service;

import app.domain.Credentials;

public interface ICredentialService extends IGeneralService<Credentials> {
    boolean exists(String login);
    Credentials getByLogin(String login);
}
