package app.service;

import app.domain.Credentials;
import app.domain.Profile;
import app.domain.utils.ProfileAndCredentialsPair;
import app.exceptions.ProfileNotFoundException;

public interface IProfileService extends IGeneralService<Profile> {
    ProfileAndCredentialsPair authentificate(Credentials credentials) throws ProfileNotFoundException;
}
