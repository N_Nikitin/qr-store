package app.service.implementations;

import app.domain.Credentials;
import app.domain.Profile;
import app.domain.utils.ProfileAndCredentialsPair;
import app.exceptions.ProfileNotFoundException;
import app.repository.IProfileRepository;
import app.service.ICredentialService;
import app.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements IProfileService{

    @Autowired
    private IProfileRepository profileRepository;

    @Autowired
    private ICredentialService credentialService;

    @Override
    public Integer save(Profile object) {
        return profileRepository.saveObject(object);
    }

    @Override
    public Profile getById(Integer id) {
        return profileRepository.getObjectById(id);
    }

    @Override
    public List<Profile> getAll() {
        return profileRepository.getObjects();
    }

    @Override
    public void updateById(Integer id, Profile object) {
        profileRepository.updateObjectById(id, object);
    }

    @Override
    public void deleteById(Integer id) {
        profileRepository.deleteObjectById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return profileRepository.exists(id);
    }

    @Override
    public ProfileAndCredentialsPair authentificate(Credentials credentials) throws ProfileNotFoundException {
        if (credentialService.exists(credentials.getLogin())) {
            Credentials foundCredentials = credentialService.getByLogin(credentials.getLogin());
            Profile foundProfile = profileRepository.getObjectById(foundCredentials.getIdProfile());
            if (foundProfile == null) {
                throw new ProfileNotFoundException();
            }
            if (!foundCredentials.getPassword().equals(credentials.getPassword())) {
                throw new ProfileNotFoundException();
            }
            return new ProfileAndCredentialsPair(foundProfile, foundCredentials);
        }
        throw new ProfileNotFoundException();
    }
}
