package app.service.implementations;

import app.domain.QRComment;
import app.repository.IQRCommentRepository;
import app.service.IQRCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QRCommentServiceImpl implements IQRCommentService {

    @Autowired
    private IQRCommentRepository qrCommentRepository;

    @Override
    public Integer save(QRComment object) {
        return qrCommentRepository.saveObject(object);
    }

    @Override
    public QRComment getById(Integer id) {
        return qrCommentRepository.getObjectById(id);
    }

    @Override
    public List<QRComment> getAll() {
        return qrCommentRepository.getObjects();
    }

    @Override
    public void updateById(Integer id, QRComment object) {
        qrCommentRepository.updateObjectById(id, object);
    }

    @Override
    public void deleteById(Integer id) {
        qrCommentRepository.deleteObjectById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return qrCommentRepository.exists(id);
    }

    @Override
    public List<QRComment> getAllByProfileId(Integer id) {
        return qrCommentRepository.getAllByProfileId(id);
    }

    @Override
    public List<QRComment> getAllByQRCodeId(Integer id) {
        return qrCommentRepository.getAllByQRCodeId(id);
    }

    @Override
    public void deleteByQRCodeId(Integer id) {
        qrCommentRepository.getAllByQRCodeId(id).forEach(t -> qrCommentRepository.deleteObjectById(t.getId()));
    }
}
