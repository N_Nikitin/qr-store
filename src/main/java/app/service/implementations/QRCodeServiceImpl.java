package app.service.implementations;

import app.domain.QRCode;
import app.repository.IQRCodeRepository;
import app.service.IQRCodeService;
import app.service.IQRCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QRCodeServiceImpl implements IQRCodeService {

    @Autowired
    private IQRCodeRepository qrCodeRepository;

    @Autowired
    private IQRCommentService qrCommentService;

    @Override
    public Integer save(QRCode object) {
        return qrCodeRepository.saveObject(object);
    }

    @Override
    public QRCode getById(Integer id) {
        return qrCodeRepository.getObjectById(id);
    }

    @Override
    public List<QRCode> getAll() {
        return qrCodeRepository.getObjects();
    }

    @Override
    public void updateById(Integer id, QRCode object) {
        qrCodeRepository.updateObjectById(id, object);
    }

    @Override
    public void deleteById(Integer id) {
        qrCodeRepository.deleteObjectById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return qrCodeRepository.exists(id);
    }

    @Override
    public List<QRCode> getAllByProfileId(Integer id) {
        return qrCodeRepository.getAllByProfileId(id);
    }

    @Override
    public void deleteByProfileId(Integer id) {
        qrCodeRepository.getAllByProfileId(id).forEach(t -> {
            qrCommentService.deleteByQRCodeId(t.getId());
            qrCodeRepository.deleteObjectById(t.getId());
        });
    }
}
