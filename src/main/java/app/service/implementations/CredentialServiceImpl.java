package app.service.implementations;

import app.domain.Credentials;
import app.repository.ICredentialRepository;
import app.service.ICredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CredentialServiceImpl implements ICredentialService {

    @Autowired
    private ICredentialRepository credentialRepository;

    @Override
    public Integer save(Credentials object) {
        return credentialRepository.saveObject(object);
    }

    @Override
    public Credentials getById(Integer id) {
        return credentialRepository.getObjectById(id);
    }

    @Override
    public List<Credentials> getAll() {
        return credentialRepository.getObjects();
    }

    @Override
    public void updateById(Integer id, Credentials object) {
        credentialRepository.updateObjectById(id, object);
    }

    @Override
    public void deleteById(Integer id) {
        credentialRepository.deleteObjectById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return credentialRepository.exists(id);
    }

    @Override
    public boolean exists(String login) {
        return credentialRepository.exists(login);
    }

    @Override
    public Credentials getByLogin(String login) {
        return credentialRepository.getByLogin(login);
    }

}
