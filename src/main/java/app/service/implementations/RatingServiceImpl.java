package app.service.implementations;

import app.domain.utils.Rating;
import app.repository.IRatingRepository;
import app.service.IRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.OptionalDouble;

@Service
public class RatingServiceImpl implements IRatingService {

    @Autowired
    private IRatingRepository ratingRepository;

    @Override
    public Double getQRCodeRatingById(Integer id) {
        List<Rating> ratingList = ratingRepository.getQRCodeRatingById(id);
        Integer result = ratingList.size();
        return Double.valueOf(result);
    }

    @Override
    public Double getQRCommentRatingById(Integer id) {
        List<Rating> ratingList = ratingRepository.getQRCommentRatingById(id);
        Integer result = ratingList.size();
        return Double.valueOf(result);
    }

    @Override
    public boolean isAlreadyVotedForQRCode(Integer idProfile, Integer idQRCode) {
        return ratingRepository.isAlreadyVotedForQRCode(idProfile, idQRCode);
    }

    @Override
    public boolean isAlreadyVotedForQRComment(Integer idProfile, Integer idQRComment) {
        return ratingRepository.isAlreadyVotedForQRComment(idProfile, idQRComment);
    }

    @Override
    public List<Rating> getAllByOwnerId(Integer id) {
        return ratingRepository.getAllByOwnerId(id);
    }

    @Override
    public void deleteAllByOwnerId(Integer id) {
        ratingRepository.deleteAllByOwnerId(id);
    }

    @Override
    public void deleteByOwnerIdAndQRCodeId(Integer ownerId, Integer qrCodeId) {
        ratingRepository.deleteByOwnerIdAndQRCodeId(ownerId, qrCodeId);
    }

    @Override
    public void deleteByOwnerIdAndQRCommentId(Integer ownerId, Integer qrCommentId) {
        ratingRepository.deleteByOwnerIdAndQRCommentId(ownerId, qrCommentId);
    }

    @Override
    public Integer save(Rating object) {
        return ratingRepository.saveObject(object);
    }

    @Override
    public Rating getById(Integer id) {
        return ratingRepository.getObjectById(id);
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getObjects();
    }

    @Override
    public void updateById(Integer id, Rating object) {
        ratingRepository.updateObjectById(id, object);
    }

    @Override
    public void deleteById(Integer id) {
        ratingRepository.deleteObjectById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return ratingRepository.exists(id);
    }
}
