package app.service;

import app.domain.utils.Rating;

import java.util.List;

public interface IRatingService extends IGeneralService<Rating> {
    Double getQRCodeRatingById(Integer id);
    Double getQRCommentRatingById(Integer id);
    boolean isAlreadyVotedForQRCode(Integer idProfile, Integer idQRCode);
    boolean isAlreadyVotedForQRComment(Integer idProfile, Integer idQRComment);
    List<Rating> getAllByOwnerId(Integer id);
    void deleteAllByOwnerId(Integer id);
    void deleteByOwnerIdAndQRCodeId(Integer ownerId, Integer qrCodeId);
    void deleteByOwnerIdAndQRCommentId(Integer ownerId, Integer qrCommentId);
}
