package app.service;

import app.domain.QRComment;

import java.util.List;

public interface IQRCommentService extends IGeneralService<QRComment> {
    List<QRComment> getAllByProfileId(Integer id);
    List<QRComment> getAllByQRCodeId(Integer id);
    void deleteByQRCodeId(Integer id);
}
