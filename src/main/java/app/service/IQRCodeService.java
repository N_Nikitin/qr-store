package app.service;

import app.domain.QRCode;

import java.util.List;

public interface IQRCodeService extends IGeneralService<QRCode> {
    List<QRCode> getAllByProfileId(Integer id);
    void deleteByProfileId(Integer id);
}
