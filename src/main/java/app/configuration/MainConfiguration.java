package app.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("src")
@Import({
    DatabaseConfiguration.class,
    TransactionConfiguration.class,
    WebConfiguration.class
})
@EnableAutoConfiguration
public class MainConfiguration {

}
