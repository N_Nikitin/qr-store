package app.repository;

import app.domain.Credentials;

public interface ICredentialRepository extends IGeneralRepository<Credentials> {
    boolean exists(String login);
    Credentials getByLogin(String login);
}
