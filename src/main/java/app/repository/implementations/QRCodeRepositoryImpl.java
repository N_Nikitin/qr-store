package app.repository.implementations;

import app.domain.QRCode;
import app.repository.IQRCodeRepository;
import app.repository.implementations.mappers.IntegerMapper;
import app.repository.implementations.mappers.QRCodeMapper;
import app.repository.implementations.queries.QRCodeSQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class QRCodeRepositoryImpl implements IQRCodeRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private QRCodeMapper qrCodeMapper;
    @Autowired
    private IntegerMapper integerMapper;
    @Autowired
    private QRCodeSQLQuery qrCodeSQLQuery;

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_RATING = "rating";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_ID_PROFILE = "idProfile";

    @Override
    public Integer saveObject(QRCode object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_NAME, object.getName());
        parameters.put(FIELD_BODY, object.getBody());
        parameters.put(FIELD_DESCRIPTION, object.getDescription());
        parameters.put(FIELD_RATING, object.getRating());
        parameters.put(FIELD_DATE, object.getDate());
        parameters.put(FIELD_ID_PROFILE, object.getProfileId());
        SqlParameterSource parameterSource = new MapSqlParameterSource(parameters);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(qrCodeSQLQuery.saveQuery(), parameterSource, keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public QRCode getObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        return namedParameterJdbcTemplate.queryForObject(qrCodeSQLQuery.getByIdQuery(), parameters, qrCodeMapper);
    }

    @Override
    public List<QRCode> getObjects() {
        return namedParameterJdbcTemplate.query(qrCodeSQLQuery.getAllQuery(), qrCodeMapper);
    }

    @Override
    public void updateObjectById(Integer id, QRCode object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, object.getId());
        parameters.put(FIELD_NAME, object.getName());
        parameters.put(FIELD_BODY, object.getBody());
        parameters.put(FIELD_DESCRIPTION, object.getDescription());
        parameters.put(FIELD_RATING, object.getRating());
        parameters.put(FIELD_DATE, object.getDate());
        parameters.put(FIELD_ID_PROFILE, object.getProfileId());
        namedParameterJdbcTemplate.update(qrCodeSQLQuery.updateQuery(), parameters);
    }

    @Override
    public void deleteObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        namedParameterJdbcTemplate.update(qrCodeSQLQuery.deleteQuery(), parameters);
    }

    @Override
    public boolean exists(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        Integer count = namedParameterJdbcTemplate.queryForObject(qrCodeSQLQuery.existsQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public List<QRCode> getAllByProfileId(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_PROFILE, id);
        return namedParameterJdbcTemplate.query(qrCodeSQLQuery.getAllByProfileIdQuery(), parameters, qrCodeMapper);
    }
}
