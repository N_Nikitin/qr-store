package app.repository.implementations.queries;

import org.springframework.stereotype.Component;

@Component
public class RatingSQLQuery {

    private static final String FIELD_ID = "id";
    private static final String FIELD_ID_QRCOMMENT = "idQRComment";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_OWNER_PROFILE = "idOwnerProfile";
    private static final String TABLE_NAME = "rating";

    private static final String QRCODE_RATING = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_QRCODE + " =:" + FIELD_ID_QRCODE + "";
    private static final String QRCOMMENT_RATING = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_QRCOMMENT + " =:" + FIELD_ID_QRCOMMENT + "";
    private static final String IS_VOTED_QRCODE = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE +
            " AND " + FIELD_ID_QRCODE + " =:" + FIELD_ID_QRCODE + "";
    private static final String IS_VOTED_QRCOMMENT = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE +
            " AND " + FIELD_ID_QRCOMMENT + " =:" + FIELD_ID_QRCOMMENT + "";
    private static final String SAVE = "INSERT INTO " + TABLE_NAME + " (" + FIELD_ID_QRCOMMENT + "," + FIELD_ID_QRCODE + "," + FIELD_ID_OWNER_PROFILE + ") " +
            "VALUES(:" + FIELD_ID_QRCOMMENT + ",:" + FIELD_ID_QRCODE + ",:" + FIELD_ID_OWNER_PROFILE + ")";

    private static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " = :" + FIELD_ID + "";
    private static final String GET_ALL = "SELECT * FROM " + TABLE_NAME + "";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET " + FIELD_ID_QRCOMMENT + "=:" + FIELD_ID_QRCOMMENT + "," + FIELD_ID_QRCODE + "=:" + FIELD_ID_QRCODE +
            "," + FIELD_ID_OWNER_PROFILE + "=:" + FIELD_ID_OWNER_PROFILE + " WHERE " + FIELD_ID + "=:" + FIELD_ID + "";

    private static final String GET_ALL_BY_OWNER_PROFILE_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE + "";
    private static final String DELETE_ALL_BY_OWNER_PROFILE_ID = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE + "";
    private static final String DELETE_BY_OWNER_PROFILE_ID_AND_QRCODE_ID = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE +
            " AND " + FIELD_ID_QRCODE + " =:" + FIELD_ID_QRCODE + "";
    private static final String DELETE_BY_OWNER_PROFILE_ID_AND_QRCOMMENT_ID = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID_OWNER_PROFILE + " =:" + FIELD_ID_OWNER_PROFILE +
            " AND " + FIELD_ID_QRCOMMENT + " =:" + FIELD_ID_QRCOMMENT + "";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";
    private static final String EXISTS = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";

    public String qrCodeRating() {
        return QRCODE_RATING;
    }

    public String qrCommentRating() {
        return QRCOMMENT_RATING;
    }

    public String isVotedForQRCode() {
        return IS_VOTED_QRCODE;
    }

    public String isVotedForQRComment() {
        return IS_VOTED_QRCOMMENT;
    }

    public String saveQuery() {
        return SAVE;
    }

    public String getByIdQuery() {
        return GET_BY_ID;
    }

    public String getAllQuery() {
        return GET_ALL;
    }

    public String updateQuery() {
        return UPDATE;
    }

    public String deleteQuery() {
        return DELETE;
    }

    public String getByOwnerProfileIdQuery() {
        return GET_ALL_BY_OWNER_PROFILE_ID;
    }

    public String deleteByOwnerProfileIdQuery() {
        return DELETE_ALL_BY_OWNER_PROFILE_ID;
    }

    public String deleteByOwnerProfileIdAndQRCodeId() {
        return DELETE_BY_OWNER_PROFILE_ID_AND_QRCODE_ID;
    }

    public String deleteByOwnerProfileIdAndQRCommentId() {
        return DELETE_BY_OWNER_PROFILE_ID_AND_QRCOMMENT_ID;
    }

    public String existsQuery() {
        return EXISTS;
    }
}
