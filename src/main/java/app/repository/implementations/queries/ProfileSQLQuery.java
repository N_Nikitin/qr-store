package app.repository.implementations.queries;

import org.springframework.stereotype.Component;

@Component
public class ProfileSQLQuery {

    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_SURNAME = "surname";
    private static final String FIELD_PHOTO = "photo";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_CITY = "city";
    private static final String TABLE_NAME = "profile";

    private static final String SAVE = "INSERT INTO " + TABLE_NAME + " (" + FIELD_NAME + "," + FIELD_SURNAME + "," + FIELD_PHOTO +
            "," + FIELD_EMAIL +"," + FIELD_COUNTRY + "," + FIELD_CITY + ") " +
            "VALUES(:" + FIELD_NAME + ",:" + FIELD_SURNAME + ",:" + FIELD_PHOTO +
            ",:" + FIELD_EMAIL +",:" + FIELD_COUNTRY + ",:" + FIELD_CITY + ")";

    private static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " = :" + FIELD_ID + "";
    private static final String GET_ALL = "SELECT * FROM " + TABLE_NAME + "";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET " + FIELD_NAME + "=:" + FIELD_NAME + "," + FIELD_SURNAME + "=:" + FIELD_SURNAME + "," + FIELD_PHOTO +
            "=:" + FIELD_PHOTO + "," + FIELD_EMAIL + "=:" + FIELD_EMAIL + "," + FIELD_COUNTRY + "=:" + FIELD_COUNTRY + "," + FIELD_CITY + "=:" + FIELD_CITY +
            " WHERE " + FIELD_ID + "=:" + FIELD_ID + "";

    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";
    private static final String EXISTS = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";

    public String saveQuery() {
        return SAVE;
    }

    public String getByIdQuery() {
        return GET_BY_ID;
    }

    public String getAllQuery() {
        return GET_ALL;
    }

    public String updateQuery() {
        return UPDATE;
    }

    public String deleteQuery() {
        return DELETE;
    }

    public String existsQuery() {
        return EXISTS;
    }

}
