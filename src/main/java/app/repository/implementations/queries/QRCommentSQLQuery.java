package app.repository.implementations.queries;

import org.springframework.stereotype.Component;

@Component
public class QRCommentSQLQuery {

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_RATING = "rating";
    private static final String TABLE_NAME = "qrcomment";

    private static final String SAVE = "INSERT INTO " + TABLE_NAME + " (" + FIELD_BODY + "," + FIELD_DATE + "," + FIELD_ID_QRCODE +
            "," + FIELD_ID_PROFILE + "," + FIELD_RATING + ") " +
            "VALUES(:" + FIELD_BODY + ",:" + FIELD_DATE + ",:" + FIELD_ID_QRCODE +
            ",:" + FIELD_ID_PROFILE + "," + FIELD_RATING + ")";

    private static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " = :" + FIELD_ID + "";
    private static final String GET_ALL = "SELECT * FROM " + TABLE_NAME + "";
    private static final String GET_ALL_BY_PROFILE_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_PROFILE + " =:" + FIELD_ID_PROFILE + "";
    private static final String GET_ALL_BY_QRCODE_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_QRCODE + " =:" + FIELD_ID_QRCODE + "";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET " + FIELD_BODY + "=:" + FIELD_BODY + "," + FIELD_DATE + "=:" + FIELD_DATE + "," + FIELD_ID_QRCODE +
            "=:" + FIELD_ID_QRCODE + "," + FIELD_ID_PROFILE + "=:" + FIELD_ID_PROFILE + "," + FIELD_RATING + "=:" + FIELD_RATING + " WHERE " + FIELD_ID + "=:" + FIELD_ID + "";

    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";
    private static final String EXISTS = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID + " =:" + FIELD_ID + "";

    public String saveQuery() {
        return SAVE;
    }

    public String getByIdQuery() {
        return GET_BY_ID;
    }

    public String getAllQuery() {
        return GET_ALL;
    }

    public String updateQuery() {
        return UPDATE;
    }

    public String deleteQuery() {
        return DELETE;
    }

    public String existsQuery() {
        return EXISTS;
    }

    public String getAllByProfileIdQuery() {
        return GET_ALL_BY_PROFILE_ID;
    }

    public String getAllByQRCodeIdQuery() {
        return GET_ALL_BY_QRCODE_ID;
    }

}
