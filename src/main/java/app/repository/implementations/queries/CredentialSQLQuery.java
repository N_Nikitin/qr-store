package app.repository.implementations.queries;

import org.springframework.stereotype.Component;

@Component
public class CredentialSQLQuery {

    private static final String FIELD_LOGIN = "login";
    private static final String FIELD_PASS = "password";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_ACCESS_LEVEL = "accessLevel";
    private static final String TABLE_NAME = "credentials";

    private static final String SAVE = "INSERT INTO " + TABLE_NAME + " (" + FIELD_LOGIN + "," + FIELD_PASS + "," + FIELD_ID_PROFILE + "," +
            FIELD_ACCESS_LEVEL + ") " + "VALUES(:" + FIELD_LOGIN + ",:" + FIELD_PASS + ",:" + FIELD_ID_PROFILE + ",:" + FIELD_ACCESS_LEVEL + ")";

    private static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_ID_PROFILE + " = :" + FIELD_ID_PROFILE + "";
    private static final String GET_BY_LOGIN = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_LOGIN + " = :" + FIELD_LOGIN + "";
    private static final String GET_ALL = "SELECT * FROM " + TABLE_NAME + "";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET " + FIELD_LOGIN + "=:" + FIELD_LOGIN + "," + FIELD_PASS + "=:" + FIELD_PASS +
            FIELD_ACCESS_LEVEL + "=:" + FIELD_ACCESS_LEVEL + " WHERE " + FIELD_ID_PROFILE + "=:" + FIELD_ID_PROFILE + "";

    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + FIELD_ID_PROFILE + " =:" + FIELD_ID_PROFILE + "";
    private static final String EXISTS = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_ID_PROFILE + " =:" + FIELD_ID_PROFILE + "";
    private static final String EXISTS_BY_LOGIN = "SELECT COUNT(*) as count FROM " + TABLE_NAME + " WHERE " + FIELD_LOGIN + " =:" + FIELD_LOGIN + "";

    public String saveQuery() {
        return SAVE;
    }

    public String getByIdQuery() {
        return GET_BY_ID;
    }

    public String getAllQuery() {
        return GET_ALL;
    }

    public String updateQuery() {
        return UPDATE;
    }

    public String deleteQuery() {
        return DELETE;
    }

    public String existsQuery() {
        return EXISTS;
    }

    public String existsByLoginQuery() {
        return EXISTS_BY_LOGIN;
    }

    public String getByLoginQuery() {
        return GET_BY_LOGIN;
    }

}
