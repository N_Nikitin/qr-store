package app.repository.implementations;

import app.domain.QRComment;
import app.repository.IQRCommentRepository;
import app.repository.implementations.mappers.IntegerMapper;
import app.repository.implementations.mappers.QRCommentMapper;
import app.repository.implementations.queries.QRCommentSQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class QRCommentRepositoryImpl implements IQRCommentRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private QRCommentMapper qrCommentMapper;
    @Autowired
    private IntegerMapper integerMapper;
    @Autowired
    private QRCommentSQLQuery qrCommentSQLQuery;

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_RATING = "rating";

    @Override
    public Integer saveObject(QRComment object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_BODY, object.getBody());
        parameters.put(FIELD_DATE, object.getDate());
        parameters.put(FIELD_ID_QRCODE, object.getQrCodeId());
        parameters.put(FIELD_ID_PROFILE, object.getProfileId());
        parameters.put(FIELD_RATING, object.getRating());
        SqlParameterSource parameterSource = new MapSqlParameterSource(parameters);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(qrCommentSQLQuery.saveQuery(), parameterSource, keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public QRComment getObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        return namedParameterJdbcTemplate.queryForObject(qrCommentSQLQuery.getByIdQuery(), parameters, qrCommentMapper);
    }

    @Override
    public List<QRComment> getObjects() {
        return namedParameterJdbcTemplate.query(qrCommentSQLQuery.getAllQuery(), qrCommentMapper);
    }

    @Override
    public void updateObjectById(Integer id, QRComment object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        parameters.put(FIELD_BODY, object.getBody());
        parameters.put(FIELD_DATE, object.getDate());
        parameters.put(FIELD_ID_QRCODE, object.getQrCodeId());
        parameters.put(FIELD_ID_PROFILE, object.getProfileId());
        parameters.put(FIELD_RATING, object.getRating());
        namedParameterJdbcTemplate.update(qrCommentSQLQuery.updateQuery(), parameters);
    }

    @Override
    public void deleteObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        namedParameterJdbcTemplate.update(qrCommentSQLQuery.deleteQuery(), parameters);
    }

    @Override
    public boolean exists(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        Integer count = namedParameterJdbcTemplate.queryForObject(qrCommentSQLQuery.existsQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public List<QRComment> getAllByProfileId(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_PROFILE, id);
        return namedParameterJdbcTemplate.query(qrCommentSQLQuery.getAllByProfileIdQuery(), parameters, qrCommentMapper);
    }

    @Override
    public List<QRComment> getAllByQRCodeId(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_QRCODE, id);
        return namedParameterJdbcTemplate.query(qrCommentSQLQuery.getAllByQRCodeIdQuery(), parameters, qrCommentMapper);
    }
}
