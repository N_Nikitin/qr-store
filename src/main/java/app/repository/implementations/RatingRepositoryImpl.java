package app.repository.implementations;

import app.domain.utils.Rating;
import app.repository.IRatingRepository;
import app.repository.implementations.mappers.IntegerMapper;
import app.repository.implementations.mappers.RatingMapper;
import app.repository.implementations.queries.RatingSQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RatingRepositoryImpl implements IRatingRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private RatingMapper ratingMapper;
    @Autowired
    private IntegerMapper integerMapper;
    @Autowired
    private RatingSQLQuery ratingSQLQuery;

    private static final String FIELD_ID = "id";
    private static final String FIELD_ID_QRCOMMENT = "idQRComment";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_OWNER_PROFILE = "idOwnerProfile";


    @Override
    public List<Rating> getQRCodeRatingById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_QRCODE, id);
        return namedParameterJdbcTemplate.query(ratingSQLQuery.qrCodeRating(), parameters, ratingMapper);
    }

    @Override
    public List<Rating> getQRCommentRatingById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_QRCOMMENT, id);
        return namedParameterJdbcTemplate.query(ratingSQLQuery.qrCommentRating(), parameters, ratingMapper);
    }

    @Override
    public boolean isAlreadyVotedForQRCode(Integer idProfile, Integer idQRCode) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, idProfile);
        parameters.put(FIELD_ID_QRCODE, idQRCode);
        Integer count = namedParameterJdbcTemplate.queryForObject(ratingSQLQuery.isVotedForQRCode(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public boolean isAlreadyVotedForQRComment(Integer idProfile, Integer idQRComment) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, idProfile);
        parameters.put(FIELD_ID_QRCOMMENT, idQRComment);
        Integer count = namedParameterJdbcTemplate.queryForObject(ratingSQLQuery.isVotedForQRComment(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public List<Rating> getAllByOwnerId(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, id);
        return namedParameterJdbcTemplate.query(ratingSQLQuery.getByOwnerProfileIdQuery(), parameters, ratingMapper);
    }

    @Override
    public void deleteAllByOwnerId(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, id);
        namedParameterJdbcTemplate.update(ratingSQLQuery.deleteByOwnerProfileIdQuery(), parameters);
    }

    @Override
    public void deleteByOwnerIdAndQRCodeId(Integer ownerId, Integer qrCodeId) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, ownerId);
        parameters.put(FIELD_ID_QRCODE, qrCodeId);
        namedParameterJdbcTemplate.update(ratingSQLQuery.deleteByOwnerProfileIdAndQRCodeId(), parameters);
    }

    @Override
    public void deleteByOwnerIdAndQRCommentId(Integer ownerId, Integer qrCommentId) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, ownerId);
        parameters.put(FIELD_ID_QRCOMMENT, qrCommentId);
        namedParameterJdbcTemplate.update(ratingSQLQuery.deleteByOwnerProfileIdAndQRCommentId(), parameters);
    }

    @Override
    public Integer saveObject(Rating object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_QRCOMMENT, object.getIdQRComment());
        parameters.put(FIELD_ID_QRCODE, object.getIdQRCode());
        parameters.put(FIELD_ID_OWNER_PROFILE, object.getIdOwnerProfile());
        namedParameterJdbcTemplate.update(ratingSQLQuery.saveQuery(), parameters);
        return object.getIdOwnerProfile();
    }

    @Override
    public Rating getObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        return namedParameterJdbcTemplate.queryForObject(ratingSQLQuery.getByIdQuery(), parameters, ratingMapper);
    }

    @Override
    public List<Rating> getObjects() {
        return namedParameterJdbcTemplate.query(ratingSQLQuery.getAllQuery(), ratingMapper);
    }

    @Override
    public void updateObjectById(Integer id, Rating object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_QRCOMMENT, object.getIdQRComment());
        parameters.put(FIELD_ID_QRCODE, object.getIdQRCode());
        parameters.put(FIELD_ID_OWNER_PROFILE, object.getIdOwnerProfile());
        namedParameterJdbcTemplate.update(ratingSQLQuery.updateQuery(), parameters);
    }

    @Override
    public void deleteObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, id);
        namedParameterJdbcTemplate.update(ratingSQLQuery.deleteQuery(), parameters);
    }

    @Override
    public boolean exists(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_OWNER_PROFILE, id);
        Integer count = namedParameterJdbcTemplate.queryForObject(ratingSQLQuery.existsQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }
}
