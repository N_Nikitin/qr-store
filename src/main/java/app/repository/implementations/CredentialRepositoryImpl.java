package app.repository.implementations;

import app.domain.Credentials;
import app.repository.ICredentialRepository;
import app.repository.implementations.mappers.CredentialMapper;
import app.repository.implementations.mappers.IntegerMapper;
import app.repository.implementations.queries.CredentialSQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CredentialRepositoryImpl implements ICredentialRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private CredentialMapper credentialMapper;
    @Autowired
    private IntegerMapper integerMapper;
    @Autowired
    private CredentialSQLQuery credentialSQLQuery;

    private static final String FIELD_LOGIN = "login";
    private static final String FIELD_PASS = "password";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_ACCESS_LEVEL = "accessLevel";

    @Override
    public Integer saveObject(Credentials object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_LOGIN, object.getLogin());
        parameters.put(FIELD_PASS, object.getPassword());
        parameters.put(FIELD_ID_PROFILE, object.getIdProfile());
        parameters.put(FIELD_ACCESS_LEVEL, object.getAccessLevel().toString().toLowerCase());
        namedParameterJdbcTemplate.update(credentialSQLQuery.saveQuery(), parameters);
        return object.getIdProfile();
    }

    @Override
    public Credentials getObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_PROFILE, id);
        return namedParameterJdbcTemplate.queryForObject(credentialSQLQuery.getByIdQuery(), parameters, credentialMapper);
    }

    @Override
    public List<Credentials> getObjects() {
        return namedParameterJdbcTemplate.query(credentialSQLQuery.getAllQuery(), credentialMapper);
    }

    @Override
    public void updateObjectById(Integer id, Credentials object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_LOGIN, object.getLogin());
        parameters.put(FIELD_PASS, object.getPassword());
        parameters.put(FIELD_ID_PROFILE, object.getIdProfile());
        parameters.put(FIELD_ACCESS_LEVEL, object.getAccessLevel().toString().toLowerCase());
        namedParameterJdbcTemplate.update(credentialSQLQuery.updateQuery(), parameters);
    }

    @Override
    public void deleteObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_PROFILE, id);
        namedParameterJdbcTemplate.update(credentialSQLQuery.deleteQuery(), parameters);
    }

    @Override
    public boolean exists(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID_PROFILE, id);
        Integer count = namedParameterJdbcTemplate.queryForObject(credentialSQLQuery.existsQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public boolean exists(String login) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_LOGIN, login);
        Integer count = namedParameterJdbcTemplate.queryForObject(credentialSQLQuery.existsByLoginQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }

    @Override
    public Credentials getByLogin(String login) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_LOGIN, login);
        return namedParameterJdbcTemplate.queryForObject(credentialSQLQuery.getByLoginQuery(), parameters, credentialMapper);
    }

}
