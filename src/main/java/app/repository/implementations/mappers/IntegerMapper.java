package app.repository.implementations.mappers;


import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class IntegerMapper implements RowMapper<Integer> {

    private static final String FIELD_COUNT = "count";

    @Override
    public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
        return resultSet.getInt(FIELD_COUNT);
    }
}
