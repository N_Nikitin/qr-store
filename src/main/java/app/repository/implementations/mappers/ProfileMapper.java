package app.repository.implementations.mappers;

import app.domain.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ProfileMapper implements RowMapper<Profile> {

    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_SURNAME = "surname";
    private static final String FIELD_PHOTO = "photo";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_CITY = "city";

    @Override
    public Profile mapRow(ResultSet resultSet, int i) throws SQLException {
        Profile profile = new Profile();
        profile.setId(resultSet.getInt(FIELD_ID));
        profile.setName(resultSet.getString(FIELD_NAME));
        profile.setSurname(resultSet.getString(FIELD_SURNAME));
        profile.setPhoto(resultSet.getString(FIELD_PHOTO));
        profile.setEmail(resultSet.getString(FIELD_EMAIL));
        profile.setCountry(resultSet.getString(FIELD_COUNTRY));
        profile.setCity(resultSet.getString(FIELD_CITY));
        return profile;
    }
}
