package app.repository.implementations.mappers;

import app.domain.utils.Rating;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RatingMapper implements RowMapper<Rating> {

    private static final String FIELD_ID = "id";
    private static final String FIELD_ID_QRCOMMENT = "idQRComment";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_OWNER_PROFILE = "idOwnerProfile";

    @Override
    public Rating mapRow(ResultSet resultSet, int i) throws SQLException {
        Rating rating = new Rating();
        rating.setId(resultSet.getInt(FIELD_ID));
        rating.setIdQRComment(resultSet.getInt(FIELD_ID_QRCOMMENT));
        rating.setIdQRCode(resultSet.getInt(FIELD_ID_QRCODE));
        rating.setIdOwnerProfile(resultSet.getInt(FIELD_ID_OWNER_PROFILE));
        return rating;
    }
}
