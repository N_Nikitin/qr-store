package app.repository.implementations.mappers;

import app.domain.Credentials;
import app.domain.utils.AccessLevel;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CredentialMapper implements RowMapper<Credentials> {

    private static final String FIELD_LOGIN = "login";
    private static final String FIELD_PASS = "password";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_ACCESS_LEVEL = "accessLevel";

    @Override
    public Credentials mapRow(ResultSet resultSet, int i) throws SQLException {
        Credentials credentials = new Credentials();
        credentials.setLogin(resultSet.getString(FIELD_LOGIN));
        credentials.setPassword(resultSet.getString(FIELD_PASS));
        credentials.setIdProfile(resultSet.getInt(FIELD_ID_PROFILE));
        credentials.setAccessLevel(AccessLevel.valueOf(resultSet.getString(FIELD_ACCESS_LEVEL).toUpperCase()));
        return credentials;
    }
}
