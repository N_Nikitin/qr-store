package app.repository.implementations.mappers;

import app.domain.QRCode;
import app.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class QRCodeMapper implements RowMapper<QRCode> {

    @Autowired
    private IProfileService profileService;

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_RATING = "rating";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_ID_PROFILE = "idProfile";

    @Override
    public QRCode mapRow(ResultSet resultSet, int i) throws SQLException {
        QRCode qrCode = new QRCode();
        qrCode.setId(resultSet.getInt(FIELD_ID));
        qrCode.setName(resultSet.getString(FIELD_NAME));
        qrCode.setBody(resultSet.getString(FIELD_BODY));
        qrCode.setDescription(resultSet.getString(FIELD_DESCRIPTION));
        qrCode.setRating(resultSet.getDouble(FIELD_RATING));
        qrCode.setDate(resultSet.getDate(FIELD_DATE));
        qrCode.setProfileId(resultSet.getInt(FIELD_ID_PROFILE));
        return qrCode;
    }
}
