package app.repository.implementations.mappers;

import app.domain.QRComment;
import app.service.IProfileService;
import app.service.IQRCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class QRCommentMapper implements RowMapper<QRComment> {

    @Autowired
    private IQRCodeService qrCodeService;
    @Autowired
    private IProfileService profileService;

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_ID_QRCODE = "idQRCode";
    private static final String FIELD_ID_PROFILE = "idProfile";
    private static final String FIELD_RATING = "rating";

    @Override
    public QRComment mapRow(ResultSet resultSet, int i) throws SQLException {
        QRComment qrComment = new QRComment();
        qrComment.setId(resultSet.getInt(FIELD_ID));
        qrComment.setBody(resultSet.getString(FIELD_BODY));
        qrComment.setDate(resultSet.getDate(FIELD_DATE));
        qrComment.setQrCodeId(resultSet.getInt(FIELD_ID_QRCODE));
        qrComment.setProfileId(resultSet.getInt(FIELD_ID_PROFILE));
        qrComment.setRating(resultSet.getDouble(FIELD_RATING));
        return qrComment;
    }
}
