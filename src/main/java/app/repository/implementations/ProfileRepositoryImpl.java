package app.repository.implementations;

import app.domain.Profile;
import app.repository.IProfileRepository;
import app.repository.implementations.mappers.IntegerMapper;
import app.repository.implementations.mappers.ProfileMapper;
import app.repository.implementations.queries.ProfileSQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProfileRepositoryImpl implements IProfileRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private ProfileMapper profileMapper;
    @Autowired
    private IntegerMapper integerMapper;
    @Autowired
    private ProfileSQLQuery profileSQLQuery;

    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_SURNAME = "surname";
    private static final String FIELD_PHOTO = "photo";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_CITY = "city";

    @Override
    public Integer saveObject(Profile object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_NAME, object.getName());
        parameters.put(FIELD_SURNAME, object.getSurname());
        parameters.put(FIELD_PHOTO, object.getPhoto());
        parameters.put(FIELD_EMAIL, object.getEmail());
        parameters.put(FIELD_COUNTRY, object.getCountry());
        parameters.put(FIELD_CITY, object.getCity());
        SqlParameterSource parameterSource = new MapSqlParameterSource(parameters);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(profileSQLQuery.saveQuery(), parameterSource, keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public Profile getObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        return namedParameterJdbcTemplate.queryForObject(profileSQLQuery.getByIdQuery(), parameters, profileMapper);
    }

    @Override
    public List<Profile> getObjects() {
        return namedParameterJdbcTemplate.query(profileSQLQuery.getAllQuery(), profileMapper);
    }

    @Override
    public void updateObjectById(Integer id, Profile object) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        parameters.put(FIELD_NAME, object.getName());
        parameters.put(FIELD_SURNAME, object.getSurname());
        parameters.put(FIELD_PHOTO, object.getPhoto());
        parameters.put(FIELD_EMAIL, object.getEmail());
        parameters.put(FIELD_COUNTRY, object.getCountry());
        parameters.put(FIELD_CITY, object.getCity());
        namedParameterJdbcTemplate.update(profileSQLQuery.updateQuery(), parameters);
    }

    @Override
    public void deleteObjectById(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        namedParameterJdbcTemplate.update(profileSQLQuery.deleteQuery(), parameters);
    }

    @Override
    public boolean exists(Integer id) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(FIELD_ID, id);
        Integer count = namedParameterJdbcTemplate.queryForObject(profileSQLQuery.existsQuery(), parameters, integerMapper);
        return count != null && count > 0;
    }
}
