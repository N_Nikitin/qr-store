package app.repository;

import app.domain.QRComment;

import java.util.List;

public interface IQRCommentRepository extends IGeneralRepository<QRComment> {
    List<QRComment> getAllByProfileId(Integer id);
    List<QRComment> getAllByQRCodeId(Integer id);
}
