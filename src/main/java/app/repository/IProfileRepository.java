package app.repository;

import app.domain.Profile;

public interface IProfileRepository extends IGeneralRepository<Profile> {

}
