package app.repository;

import app.domain.QRCode;

import java.util.List;

public interface IQRCodeRepository extends IGeneralRepository<QRCode> {
    List<QRCode> getAllByProfileId(Integer id);
}
