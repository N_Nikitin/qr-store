package app.repository;

import app.domain.utils.Rating;

import java.util.List;

public interface IRatingRepository extends IGeneralRepository<Rating> {
    List<Rating> getQRCodeRatingById(Integer id);
    List<Rating> getQRCommentRatingById(Integer id);
    boolean isAlreadyVotedForQRCode(Integer idProfile, Integer idQRCode);
    boolean isAlreadyVotedForQRComment(Integer idProfile, Integer idQRComment);
    List<Rating> getAllByOwnerId(Integer id);
    void deleteAllByOwnerId(Integer id);
    void deleteByOwnerIdAndQRCodeId(Integer ownerId, Integer qrCodeId);
    void deleteByOwnerIdAndQRCommentId(Integer ownerId, Integer qrCommentId);
}
