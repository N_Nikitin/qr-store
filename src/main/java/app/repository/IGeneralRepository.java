package app.repository;

import java.util.List;

public interface IGeneralRepository<T> {
    Integer saveObject(T object);
    T getObjectById(Integer id);
    List<T> getObjects();
    void updateObjectById(Integer id, T object);
    void deleteObjectById(Integer id);
    boolean exists(Integer id);
}
