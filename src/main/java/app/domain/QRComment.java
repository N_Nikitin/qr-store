package app.domain;

import java.util.Date;

public class QRComment {

    private Integer id;
    private Integer qrCodeId;
    private Integer profileId;
    private String body;
    private Date date;
    private Double rating = 0d;

    public QRComment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getQrCodeId() {
        return qrCodeId;
    }

    public void setQrCodeId(Integer qrCodeId) {
        this.qrCodeId = qrCodeId;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QRComment qrComment = (QRComment) o;

        if (id != null ? !id.equals(qrComment.id) : qrComment.id != null) return false;
        if (qrCodeId != null ? !qrCodeId.equals(qrComment.qrCodeId) : qrComment.qrCodeId != null) return false;
        if (profileId != null ? !profileId.equals(qrComment.profileId) : qrComment.profileId != null) return false;
        if (body != null ? !body.equals(qrComment.body) : qrComment.body != null) return false;
        if (date != null ? !date.equals(qrComment.date) : qrComment.date != null) return false;
        return rating != null ? rating.equals(qrComment.rating) : qrComment.rating == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (qrCodeId != null ? qrCodeId.hashCode() : 0);
        result = 31 * result + (profileId != null ? profileId.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QRComment{" +
                "id=" + id +
                ", qrCodeId=" + qrCodeId +
                ", profileId=" + profileId +
                ", body='" + body + '\'' +
                ", date=" + date +
                ", rating=" + rating +
                '}';
    }
}
