package app.domain.utils;

import app.domain.Credentials;
import app.domain.Profile;

public class ProfileAndCredentialsPair {
    private Profile profile;
    private Credentials credentials;

    public ProfileAndCredentialsPair(Profile profile, Credentials credentials) {
        this.profile = profile;
        this.credentials = credentials;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
