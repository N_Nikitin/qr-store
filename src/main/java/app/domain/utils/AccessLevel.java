package app.domain.utils;

public enum AccessLevel {
    ADMIN,
    USER
}
