package app.domain.utils;

public class Rating {

    private Integer id;
    private Integer idQRCode;
    private Integer idQRComment;
    private Integer idOwnerProfile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdQRComment() {
        return idQRComment;
    }

    public void setIdQRComment(Integer idQRComment) {
        this.idQRComment = idQRComment;
    }

    public Integer getIdOwnerProfile() {
        return idOwnerProfile;
    }

    public void setIdOwnerProfile(Integer idOwnerProfile) {
        this.idOwnerProfile = idOwnerProfile;
    }

    public Rating() {
    }

    public Rating(Integer idQRCode, Integer idQRComment, Integer idOwnerProfile) {
        this.idQRCode = idQRCode;
        this.idQRComment = idQRComment;
        this.idOwnerProfile = idOwnerProfile;
    }

    public Integer getIdQRCode() {
        return idQRCode;
    }

    public void setIdQRCode(Integer idQRCode) {
        this.idQRCode = idQRCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (id != null ? !id.equals(rating.id) : rating.id != null) return false;
        if (idQRCode != null ? !idQRCode.equals(rating.idQRCode) : rating.idQRCode != null) return false;
        if (idQRComment != null ? !idQRComment.equals(rating.idQRComment) : rating.idQRComment != null) return false;
        return idOwnerProfile != null ? idOwnerProfile.equals(rating.idOwnerProfile) : rating.idOwnerProfile == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (idQRCode != null ? idQRCode.hashCode() : 0);
        result = 31 * result + (idQRComment != null ? idQRComment.hashCode() : 0);
        result = 31 * result + (idOwnerProfile != null ? idOwnerProfile.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "id=" + id +
                ", idQRCode=" + idQRCode +
                ", idQRComment=" + idQRComment +
                ", idOwnerProfile=" + idOwnerProfile +
                '}';
    }
}
