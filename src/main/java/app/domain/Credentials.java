package app.domain;

import app.domain.utils.AccessLevel;

public class Credentials {

    private String login;
    private String password;
    private Integer idProfile;
    private AccessLevel accessLevel;

    public Credentials(){
    }

    public Credentials(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Credentials(String login, String password, Integer idProfile, AccessLevel accessLevel) {
        this.login = login;
        this.password = password;
        this.idProfile = idProfile;
        this.accessLevel = accessLevel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(Integer idProfile) {
        this.idProfile = idProfile;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", idProfile=" + idProfile +
                ", accessLevel=" + accessLevel +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credentials that = (Credentials) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (idProfile != null ? !idProfile.equals(that.idProfile) : that.idProfile != null) return false;
        return accessLevel == that.accessLevel;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (idProfile != null ? idProfile.hashCode() : 0);
        result = 31 * result + (accessLevel != null ? accessLevel.hashCode() : 0);
        return result;
    }
}
