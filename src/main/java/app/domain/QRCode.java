package app.domain;

import java.util.Date;

public class QRCode {

    private Integer id;
    private String body;
    private String name;
    private String description;
    private Double rating = 0d;
    private Integer profileId;
    private Date date = new Date();

    public QRCode() {
    }

    public QRCode(Integer id, String body, String name, String description) {
        this.id = id;
        this.body = body;
        this.name = name;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QRCode qrCode = (QRCode) o;

        if (id != null ? !id.equals(qrCode.id) : qrCode.id != null) return false;
        if (body != null ? !body.equals(qrCode.body) : qrCode.body != null) return false;
        if (name != null ? !name.equals(qrCode.name) : qrCode.name != null) return false;
        if (description != null ? !description.equals(qrCode.description) : qrCode.description != null) return false;
        if (rating != null ? !rating.equals(qrCode.rating) : qrCode.rating != null) return false;
        if (profileId != null ? !profileId.equals(qrCode.profileId) : qrCode.profileId != null) return false;
        return date != null ? date.equals(qrCode.date) : qrCode.date == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (profileId != null ? profileId.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QRCode{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", rating=" + rating +
                ", profileId=" + profileId +
                ", date=" + date +
                '}';
    }
}
