package app.controllers;

import app.domain.QRCode;
import app.exceptions.QRCodeNotFoundException;
import app.managers.ProfileManager;
import app.service.IQRCodeService;
import app.service.IQRCommentService;
import app.service.IRatingService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/qrcode")
public class QRCodeController {

    @Autowired
    private IQRCodeService qrCodeService;

    @Autowired
    private ProfileManager profileManager;

    @Autowired
    private IRatingService ratingService;

    @Autowired
    private IQRCommentService qrCommentService;

    @PostMapping("/create")
    @ResponseBody
    private String create(@RequestBody QRCode qrCode) {
        qrCode.setProfileId(profileManager.getProfile().getId());
        qrCode.setRating(0d);
        qrCode.setDate(new Date());
        qrCode.setId(null);
        qrCodeService.save(qrCode);
        return "true";
    }

    @PostMapping("/update")
    @ResponseBody
    private String update(@RequestBody QRCode qrCode) {
        QRCode qrCodeOld = qrCodeService.getById(qrCode.getId());
        if (qrCodeOld.getProfileId().equals(profileManager.getProfile().getId())) {
            qrCodeOld.setBody(qrCode.getBody());
            qrCodeOld.setDescription(qrCode.getDescription());
            qrCodeOld.setName(qrCode.getName());
            qrCodeService.updateById(qrCodeOld.getId(), qrCodeOld);
            return new Gson().toJson(qrCodeService.getById(qrCodeOld.getId()));
        } else return "false";
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    private String getById(@PathVariable Integer id) throws QRCodeNotFoundException {
        QRCode qrCode = qrCodeService.getById(id);
        if (qrCode != null) {
            qrCode.setRating(ratingService.getQRCodeRatingById(id));
            return new Gson().toJson(qrCode);
        } else throw new QRCodeNotFoundException();
    }

    @GetMapping("/delete/{id}")
    @ResponseBody
    private String delete(@PathVariable Integer id) {
        QRCode qrCode = qrCodeService.getById(id);
        if (profileManager.getProfile().getId().equals(qrCode.getProfileId())) {
            qrCommentService.deleteByQRCodeId(id);
            qrCodeService.deleteById(id);
            return "true";
        } else return "false";
    }

    @GetMapping("/get/all")
    @ResponseBody
    private String getAll() {
        List<QRCode> qrCodes = qrCodeService.getAll();
        qrCodes.forEach(c -> c.setRating(ratingService.getQRCodeRatingById(c.getId())));
        return new Gson().toJson(qrCodes);
    }

    @GetMapping("/get/mine")
    @ResponseBody
    private String getMine() {
        List<QRCode> qrCodes = qrCodeService.getAllByProfileId(profileManager.getProfile().getId());
        qrCodes.forEach(c -> c.setRating(ratingService.getQRCodeRatingById(c.getId())));
        return new Gson().toJson(qrCodes);
    }

    @ExceptionHandler(QRCodeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserAlreadyExistsException(Model model) {
        return "false";
    }

}
