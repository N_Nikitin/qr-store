package app.controllers;

import app.domain.Credentials;
import app.domain.utils.ProfileAndCredentialsPair;
import app.exceptions.ProfileNotFoundException;
import app.managers.ProfileManager;
import app.service.IProfileService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
public class AuthController {

    @Autowired
    private IProfileService profileService;

    @Autowired
    private ProfileManager profileManager;

    @PostMapping("/auth")
    @ResponseBody
    public String authorize(@RequestBody Credentials credentials, BindingResult bindingResult) throws ProfileNotFoundException {
        if (profileManager.getProfile() == null) {
            ProfileAndCredentialsPair foundValues = profileService.authentificate(credentials);
            profileManager.setProfile(foundValues.getProfile());
            profileManager.setCredentials(foundValues.getCredentials());
            return new Gson().toJson(foundValues.getProfile());
        } else if (profileManager.getCredentials().getLogin().equals(credentials.getLogin())) {
            return new Gson().toJson(profileManager.getProfile());
        } else return "false";
    }

    @GetMapping("/auth/{login}/{password}")
    @ResponseBody
    public String authorizeToDel(@PathVariable String login, @PathVariable String password) throws ProfileNotFoundException {
        if (profileManager.getProfile() == null) {
            ProfileAndCredentialsPair foundValues = profileService.authentificate(new Credentials(login, password));
            profileManager.setProfile(foundValues.getProfile());
            profileManager.setCredentials(foundValues.getCredentials());
            return new Gson().toJson(foundValues.getProfile());
        } else if (profileManager.getCredentials().getLogin().equals(login)) {
            return "true";
        } else return "false";
    }

    @GetMapping("/logout")
    @ResponseBody
    public String logout(HttpSession session) {
        session.invalidate();
        return "true";
    }

}
