package app.controllers;

import app.domain.Credentials;
import app.domain.Profile;
import app.domain.utils.AccessLevel;
import app.domain.utils.ProfileAndCredentialsRegistrationEntity;
import app.exceptions.ProfileAlreadyExistsException;
import app.managers.ProfileManager;
import app.service.ICredentialService;
import app.service.IProfileService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegistrationController {

    @Autowired
    private ICredentialService credentialService;

    @Autowired
    private IProfileService profileService;

    @Autowired
    private ProfileManager profileManager;

    @PostMapping("/register")
    @ResponseBody
    public String register(@RequestBody ProfileAndCredentialsRegistrationEntity entity, BindingResult bindingResult) throws ProfileAlreadyExistsException {
        Profile profile = buildProfile(entity);
        Credentials credentials = new Credentials(entity.getLogin(), entity.getPassword());
        if (profileManager.getProfile() != null) {
            return "false";
        }
        if (!credentialService.exists(credentials.getLogin())) {
            profile.setId(null);
            Integer profileId = profileService.save(profile);
            credentials.setIdProfile(profileId);
            credentials.setAccessLevel(AccessLevel.USER);
            Integer credentialsId = credentialService.save(credentials);
            Credentials newCredentials = credentialService.getById(credentialsId);
            Profile newProfile = profileService.getById(profileId);
            profileManager.setCredentials(newCredentials);
            profileManager.setProfile(newProfile);
            return new Gson().toJson(newProfile);
        }
        throw new ProfileAlreadyExistsException();
    }

    private Profile buildProfile(ProfileAndCredentialsRegistrationEntity entity) {
        Profile profile = new Profile();
        profile.setPhoto(entity.getPhoto());
        profile.setCity(entity.getCity());
        profile.setCountry(entity.getCountry());
        profile.setEmail(entity.getEmail());
        profile.setName(entity.getName());
        profile.setSurname(entity.getSurname());
        return profile;
    }

    @ExceptionHandler(ProfileAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserAlreadyExistsException(Model model) {
        return "false";
    }
}
