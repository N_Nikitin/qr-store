package app.controllers;

import app.domain.Profile;
import app.exceptions.ProfileNotFoundException;
import app.managers.ProfileManager;
import app.service.ICredentialService;
import app.service.IProfileService;
import app.service.IQRCodeService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private ICredentialService credentialService;

    @Autowired
    private IProfileService profileService;

    @Autowired
    private ProfileManager profileManager;

    @Autowired
    private IQRCodeService qrCodeService;

    @PostMapping("/update")
    @ResponseBody
    private String update(@RequestBody Profile profile) throws ProfileNotFoundException {
        Integer profileId = credentialService.getByLogin(profileManager.getCredentials().getLogin()).getIdProfile();
        profile.setId(profileId);
        profileService.updateById(profileId, profile);
        return new Gson().toJson(profileService.getById(profileId));
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    private String getById(@PathVariable Integer id) throws ProfileNotFoundException {
        Profile profile = profileService.getById(id);
        if (profile != null) {
            return new Gson().toJson(profile);
        } else throw new ProfileNotFoundException();
    }

    @GetMapping("/delete")
    @ResponseBody
    private String delete(HttpSession session) {
        qrCodeService.deleteByProfileId(profileManager.getProfile().getId());
        credentialService.deleteById(profileManager.getProfile().getId());
        profileService.deleteById(profileManager.getProfile().getId());
        session.invalidate();
        return "true";
    }

    @GetMapping("/get/all")
    @ResponseBody
    private String getAll() {
        return new Gson().toJson(profileService.getAll());
    }

    @ExceptionHandler(ProfileNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserAlreadyExistsException(Model model) {
        return "false";
    }
}
