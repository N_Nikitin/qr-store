package app.controllers;

import app.domain.QRComment;
import app.exceptions.QRCommentNotFoundException;
import app.managers.ProfileManager;
import app.service.IQRCommentService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/qrcomment")
public class QRCommentController {

    @Autowired
    private IQRCommentService qrCommentService;

    @Autowired
    private ProfileManager profileManager;

    @PostMapping("/create")
    @ResponseBody
    private String create(@RequestBody QRComment qrComment) {
        qrComment.setProfileId(profileManager.getProfile().getId());
        qrComment.setDate(new Date());
        qrComment.setId(null);
        qrCommentService.save(qrComment);
        return "true";
    }

    @PostMapping("/update")
    @ResponseBody
    private String update(@RequestBody QRComment qrComment) {
        QRComment qrCommentOld = qrCommentService.getById(qrComment.getId());
        if (qrCommentOld.getProfileId().equals(profileManager.getProfile().getId())) {
            qrCommentOld.setBody(qrComment.getBody());
            qrCommentService.updateById(qrCommentOld.getId(), qrCommentOld);
            return new Gson().toJson(qrCommentService.getById(qrComment.getId()));
        } else return "false";
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    private String getById(@PathVariable Integer id) throws QRCommentNotFoundException {
        QRComment qrComment = qrCommentService.getById(id);
        if (qrComment != null) {
            return new Gson().toJson(qrComment);
        } else throw new QRCommentNotFoundException();
    }

    @GetMapping("/delete/{id}")
    @ResponseBody
    private String delete(@PathVariable Integer id) {
        QRComment qrComment = qrCommentService.getById(id);
        if (profileManager.getProfile().getId().equals(qrComment.getProfileId())) {
            qrCommentService.deleteById(id);
            return "true";
        } else return "false";
    }

    @GetMapping("/get/all")
    @ResponseBody
    private String getAll() {
        List<QRComment> qrComments = qrCommentService.getAll();
        return new Gson().toJson(qrComments);
    }

    @GetMapping("/get/qrcode/{id}")
    @ResponseBody
    private String getAllByQRCodeId(@PathVariable Integer id) {
        List<QRComment> qrComments = qrCommentService.getAllByQRCodeId(id);
        return new Gson().toJson(qrComments);
    }

    @GetMapping("/get/mine")
    @ResponseBody
    private String getMine() {
        List<QRComment> qrComments = qrCommentService.getAllByProfileId(profileManager.getProfile().getId());
        return new Gson().toJson(qrComments);
    }

    @ExceptionHandler(QRCommentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserAlreadyExistsException(Model model) {
        return "false";
    }

}
