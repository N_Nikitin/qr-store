package app.controllers;

import app.domain.QRCode;
import app.domain.QRComment;
import app.domain.utils.Rating;
import app.managers.ProfileManager;
import app.service.IQRCodeService;
import app.service.IQRCommentService;
import app.service.IRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rating")
public class RatingController {
    @Autowired
    private IRatingService ratingService;
    @Autowired
    private ProfileManager profileManager;
    @Autowired
    private IQRCodeService qrCodeService;
    @Autowired
    private IQRCommentService qrCommentService;

    @PostMapping("/create/qrcode")
    public String createForQRCode(@RequestBody QRCode qrCode) {
        if (!ratingService.isAlreadyVotedForQRCode(profileManager.getProfile().getId(), qrCode.getId())) {
            ratingService.save(new Rating(qrCode.getId(), null, profileManager.getProfile().getId()));
            QRCode qrCodeFromDB = qrCodeService.getById(qrCode.getId());
            qrCodeFromDB.setRating(ratingService.getQRCodeRatingById(qrCode.getId()));
            qrCodeService.updateById(qrCodeFromDB.getId(), qrCodeFromDB);
            return "true";
        } else {
            return "false";
        }
    }

    @PostMapping("/create/qrcomment")
    public String createForQRComment(@RequestBody QRComment qrComment) {
        if (!ratingService.isAlreadyVotedForQRComment(profileManager.getProfile().getId(), qrComment.getId())) {
            ratingService.save(new Rating(null, qrComment.getId(), profileManager.getProfile().getId()));
            QRComment qrCommentFromDB = qrCommentService.getById(qrComment.getId());
            qrCommentFromDB.setRating(ratingService.getQRCommentRatingById(qrComment.getId()));
            qrCommentService.updateById(qrCommentFromDB.getId(), qrCommentFromDB);
            return "true";
        } else {
            return "false";
        }
    }

    @GetMapping("/delete/all")
    @ResponseBody
    private String delete() {
        List<Rating> ratingList = ratingService.getAllByOwnerId(profileManager.getProfile().getId());
        ratingService.deleteAllByOwnerId(profileManager.getProfile().getId());
        ratingList.forEach(r -> {
            if (r.getIdQRCode() != null) {
                QRCode qrCodeFromDB = qrCodeService.getById(r.getIdQRCode());
                qrCodeFromDB.setRating(ratingService.getQRCodeRatingById(r.getIdQRCode()));
                qrCodeService.updateById(qrCodeFromDB.getId(), qrCodeFromDB);
            }
            if (r.getIdQRComment() != null) {
                QRComment qrCommentFromDB = qrCommentService.getById(r.getIdQRComment());
                qrCommentFromDB.setRating(ratingService.getQRCommentRatingById(r.getIdQRComment()));
                qrCommentService.updateById(qrCommentFromDB.getId(), qrCommentFromDB);
            }
        });
        return "true";
    }

    @GetMapping("/delete/qrcode/{id}")
    @ResponseBody
    private String deleteQRCode(@PathVariable Integer id) {
        ratingService.deleteByOwnerIdAndQRCodeId(profileManager.getProfile().getId(), id);
        QRCode qrCodeFromDB = qrCodeService.getById(id);
        qrCodeFromDB.setRating(ratingService.getQRCodeRatingById(id));
        qrCodeService.updateById(qrCodeFromDB.getId(), qrCodeFromDB);
        return "true";
    }

    @GetMapping("/delete/qrcomment/{id}")
    @ResponseBody
    private String deleteQRComment(@PathVariable Integer id) {
        ratingService.deleteByOwnerIdAndQRCommentId(profileManager.getProfile().getId(), id);
        QRComment qrCommentFromDB = qrCommentService.getById(id);
        qrCommentFromDB.setRating(ratingService.getQRCommentRatingById(id));
        qrCommentService.updateById(qrCommentFromDB.getId(), qrCommentFromDB);
        return "true";
    }

}
